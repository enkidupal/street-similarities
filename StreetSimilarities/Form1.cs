﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Linq.Expressions;
using System.Globalization;
using Wintellect.PowerCollections;
using System.Threading;


namespace StreetSimilarities
{
    public delegate void ProgressBarEventHandler(int value, int max,int step);
    public delegate void ProgressBarEventStepHandler();
    public partial class Form1 : Form
    {
        static volatile int howManyCalled = 0;
        //private string PathToStreetList; //wskazanie pliku z ktorego pochodzi lista ulic
        private string PathToCandidateList; //wskazanie pliku z którego pochodzi lista kandydatów
        private Library.StreetDictonary myStrDict;
        private Library.StreetDictonary mycandidDict;
        OrderedMultiDictionary<Library.Street, Library.StreetWithCof> myStreetPairs;
        OrderedMultiDictionary<Library.Street, Library.StreetWithCof> approvedStreetPairs;
        HashSet<int> checkedItems;
        private int MaxStreetOutputPerCandidate = 10;

        

        //private float cutOffparam = (float)Properties.Settings.Default["cutOffparam"];//ile gorsze od najlepszego moga byc wyswietlane dopasowania
        private float cutOffparam = 0.4f;//ile gorsze od najlepszego moga byc wyswietlane dopasowania
        private float absoluteCutoff = (float)Properties.Settings.Default["absoluteCutoff"];

        static int numOfProcs;
        public Form1()
        {
            InitializeComponent();
            numOfProcs = Environment.ProcessorCount;
            openFileDialog1.InitialDirectory = System.Reflection.Assembly.GetEntryAssembly().Location;
            myStrDict = new Library.StreetDictonary();
            mycandidDict = new Library.StreetDictonary();
            myStreetPairs = new OrderedMultiDictionary<Library.Street, Library.StreetWithCof>(true);
            approvedStreetPairs = new OrderedMultiDictionary<Library.Street, StreetSimilarities.Library.StreetWithCof>(true);
            checkedItems = new HashSet<int>();
            listView1.Columns[0].Width = Convert.ToInt32(listView1.Width * 0.42);
            listView1.Columns[1].Width = Convert.ToInt32(listView1.Width * 0.42);
            listView1.Columns[2].Width = Convert.ToInt32(listView1.Width * 0.16);
            Library.LoadData.ProgressBarEvent += new ProgressBarEventHandler(ProgressBarEvent);
            Library.LoadData.ProgressBarStepEvent += new ProgressBarEventStepHandler(ProgressBarStepEvent);

            Library.NameComparer.ProgressBarEvent +=new ProgressBarEventHandler(ProgressBarEvent);
            Library.NameComparer.ProgressBarStepEvent += new ProgressBarEventStepHandler(ProgressBarStepEvent);
            listView1.ItemChecked += new ItemCheckedEventHandler(listView1_ItemChecked);
        }

        //calculate stuff
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ComputeStreetDictionary();
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }
        }

        private void listView1_SizeChanged(object sender, EventArgs e)
        {
            listView1.Columns[0].Width = Convert.ToInt32(listView1.Width * 0.42);
            listView1.Columns[1].Width = Convert.ToInt32(listView1.Width * 0.42);
            listView1.Columns[2].Width = Convert.ToInt32(listView1.Width * 0.16);     
        }
    
        private void LoadListBoxWithStreetNames(ListBox lb, Library.StreetDictonary stD)
        {
            if (stD == null)
                return;
            lb.Items.Clear();
            var arr = (from p in stD.streetNames
                      orderby p
                      select p.streetName).ToArray();
            lb.BeginUpdate();    
            lb.Items.AddRange(arr);
            lb.EndUpdate();
                            
        }

        private void updateSlownikiLabels(String pre, Label label, Library.StreetDictonary stD) {
            if (stD == null)
                return;
            label.Text = pre + stD.streetNames.Count;
        }

        /// <summary>
        /// Ładuje listwiew wpisami ze słownika
        /// </summary>
        /// <param name="lb"></param>
        /// <param name="val"></param>
        private void LoadListViewWithStreetNames(ListView lb,
                   OrderedMultiDictionary<Library.Street, Library.StreetWithCof> val)
        {

            if (val == null)
                return;
            lb.Items.Clear();
            checkedItems = new HashSet<int>();
            string[] it = new string[3];
            listView1.Items.Clear();
            int count = 0;
            Library.StreetWithCof[] arr;
            float bestCofCutOff;
            ListViewItem item;
            foreach (var StrPairWithCoeff in val)
            {
                //bestCofCutOff = StrPairWithCoeff.Value.First().cof * (float)Properties.Settings.Default["cutOffparam"];
                bestCofCutOff = StrPairWithCoeff.Value.First().cof * cutOffparam;
                count = 0;
                arr = val[StrPairWithCoeff.Key].ToArray();
                if (Math.Abs(arr.First().cof - 1.0F) < Library.NameComparer.epsilon) //if First matched 100%
                {
                    it[0] = StrPairWithCoeff.Key.streetName;
                    it[1] = arr[0].street.streetName;
                    it[2] = arr[0].cof.ToString("0.00.00000");
                    item = new ListViewItem(it);
                    item.ForeColor = Color.Blue;
                    listView1.Items.Add(item);
                    continue;
                }
                bool begin = true;
                foreach (var strPair in val[StrPairWithCoeff.Key].ToArray())
                {
                    if (strPair.cof < bestCofCutOff)
                        break;
                    if (count++ >= MaxStreetOutputPerCandidate)
                        break;
                    
                    it[0] = StrPairWithCoeff.Key.streetName;
                    it[1] = strPair.street.streetName;
                    it[2] = strPair.cof.ToString("0.00.00000");
                    item = new ListViewItem(it);
                    if (arr.First().street == strPair.street)
                        item.ForeColor = Color.Blue;

                    if (begin)
                    {
                        listView1.Items.Add(item);
                        checkedItems.Add(item.Index);
                        item.Checked = true;
                        begin = false;
                    }
                    else { listView1.Items.Add(item); checkedItems.Add(item.Index); }
                }
            }
        }

        private void btnDictLoad_Click(object sender, EventArgs e)
        {
            LoadDataFromExcel();  
            
        }
        private void btnCandLoad_Click(object sender, EventArgs e)
        {
            CandidateLoad();
        }

        private void CandidateLoad()
        {
            openFileDialog1.Filter = @"Excel,CSV(*.xls, *.csv)|*.xls; *.csv;|All files|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.FileName = "";
            openFileDialog1.Title = "Wybierz plik ze kandydatami";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                mycandidDict = new Library.StreetDictonary();
                try
                {
                    mycandidDict = Library.LoadData.GetExcel(openFileDialog1.FileName);
                    //LoadListBoxWithStreetNames(kandydaci, mycandidDict);
                    updateSlownikiLabels("Liczba kandydatów w pliku: ", candidateLabel, mycandidDict);
                    PathToCandidateList = openFileDialog1.FileName;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "Blad", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                //MessageBox.Show("Załadowano " + kandydaci.Items.Count + " ulic do sprawdzenia", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show("Załadowano " + mycandidDict.streetNames.Count + " ulic do sprawdzenia", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void LoadDataFromExcel()
        {
            //openFileDialog1.Filter = @"Excel files|*.xls|CSV files|*.csv|All files|*.*";
            //openFileDialog1.FilterIndex = 2;
            openFileDialog1.Filter = @"Excel,CSV(*.xls, *.csv)|*.xls; *.csv;|All files|*.*";
            openFileDialog1.FilterIndex = 0;
            openFileDialog1.FileName = "";
            openFileDialog1.Title = "Wybierz plik ze słownikiem";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                myStrDict = new Library.StreetDictonary();
                try
                {
                    myStrDict = Library.LoadData.GetExcel(openFileDialog1.FileName);
                    //LoadListBoxWithStreetNames(słownik, myStrDict);
                    updateSlownikiLabels("Liczba wpisów w słowniku: ", slownikLabel, myStrDict);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString(), "Blad", MessageBoxButtons.OK, MessageBoxIcon.Error);     
                }
                MessageBox.Show("Załadowano " + myStrDict.streetNames.Count + " wpisów w słowniku", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }
        private void ComputeStreetDictionary()
        {
            //BackgroundWorker bw = new BackgroundWorker();
            //bw.WorkerReportsProgress = true;
            //bw.DoWork += ComputeDictonaryParallel;
            //numOfProcs = 1;
            int originalPairCount = 0;
            int trueNumofThreads = numOfProcs;
            if (myStrDict.streetNames.Count == 0 || mycandidDict.streetNames.Count == 0)
            {
                MessageBox.Show("Słownik pusty", "!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return; 
            }

            if (mycandidDict.streetNames.Count == 0)
            {
                MessageBox.Show("brak ulic do sprawdzenia! ", "!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            //trueNumofThreads = 8;/////////////////////DEBUG!!!!!!!!!
            if (MessageBox.Show(String.Format("Odpalamy? Najwyżej {0} kombinacji do sprawdzenia...",
                (myStrDict.streetNames.Count * mycandidDict.streetNames.Count).ToString()), "GO?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
                return;
            toolStripStatusLabel2.Text = String.Format(String.Format("Mielę dane {0} wpisów w słowniku {1} ulic do sprawdzenia...",
                                                myStrDict.streetNames.Count, mycandidDict.streetNames.Count));
            statusStrip1.Refresh();
            myStreetPairs.Clear();
            ProgressBarEvent(0, mycandidDict.streetNames.Count, 1);
            DateTime tt = DateTime.Now;
            Library.Street[] fff = (from d in mycandidDict.streetNames
                           select d).ToArray();

            Library.StreetDictonary[] partialDicts = new Library.StreetDictonary[trueNumofThreads];
            Thread[] th = new Thread[trueNumofThreads];
            int tempI = 0;
            foreach (IEnumerable<Library.Street> ttt in fff.Slice((int)Math.Ceiling((double)fff.Length /trueNumofThreads) ) )
            {
                if (tempI >= trueNumofThreads)
                    break;
                partialDicts[tempI++] = new Library.StreetDictonary(ttt);
            }

            //Parallel Start
            for (int i = 0; i < partialDicts.Length; i++)
            {
                th[i] = new Thread(ComputeDictonaryParallel);
                th[i].Name = "Worker " + i.ToString();
                th[i].Start(partialDicts[i]);           
            }
            for (int i = 0; i < trueNumofThreads; i++)
            {
                th[i].Join();
            }
            //Parallel END

            //MessageBox.Show("sss");
            originalPairCount = myStreetPairs.KeyValuePairs.Count ;
            var items = myStreetPairs.Where(k => Math.Abs(k.Value.ToArray()[0].cof - 1) < Library.NameComparer.epsilon).ToArray();
            foreach (var y in items){
                approvedStreetPairs.Add(y);
                myStreetPairs.Remove(y.Key);
            }
           
            DateTime rr = DateTime.Now;
            var p = rr - tt;
            string minus = String.Format("Total Time:{0} Minut, {1}.{2} sekund ", p.Minutes, p.Seconds, p.Milliseconds);
            toolStripStatusLabel1.Text = minus;
            toolStripStatusLabel2.Text = String.Format("Liczba par > cutoff:{0}", originalPairCount);
            LoadListViewWithStreetNames(listView1, myStreetPairs);
            //checkedItems.Clear();
            listView1.Focus();
            listView1.SelectedIndices.Add(0);
        }

        //parallel version of Computing 
        private void ComputeDictonaryParallel(object data)
        {
            Library.StreetDictonary myPartialCand = (Library.StreetDictonary)data;
            OrderedMultiDictionary<Library.Street, Library.StreetWithCof> partialDict = new OrderedMultiDictionary<Library.Street, StreetSimilarities.Library.StreetWithCof>(true);
            Library.StreetDictonary localThreadDict;
            
            lock (myStrDict) {
                localThreadDict = (Library.StreetDictonary)( myStrDict.Clone() );
            }
          
            partialDict = Library.NameComparer.GetStreetPairsWithCoefficent(localThreadDict,
                            myPartialCand, cutOffparam, absoluteCutoff);// absoluteCutoff);
            lock (myStreetPairs)
            {
                foreach (var item in partialDict)
                {
                    myStreetPairs.Add(item);
                }
            }
        }

        //zapisz zmiany do pliku
        private void WriteChanges()
        {
            //foreach (var item in myStreetPairs)
            //{
            //    approvedStreetPairs.Add(item);
            //}
            //myStreetPairs.Clear();
            Library.LoadData.LoadToCSV(PathToCandidateList, approvedStreetPairs, myStreetPairs, true, cutOffparam, 10);
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            float tempD =0.0F;
            if (float.TryParse(textBox1.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tempD) && tempD >= 0.0 && tempD <= 1.0)
            {
                Properties.Settings.Default["cutOffparam"] = tempD;
                cutOffparam = tempD;
            }
        }
        /// <summary>
        /// Dodaje zaznaczone wpisy slownikowe do wpisów zaaprobowanych
        /// </summary>
        private void AddToApproved()
        {
            string dictStrName;
            string candStrName;
            float cof;//,cofFromDict;
            Library.Street dictStreet, candStreet;
            //ListViewItem item;
            bool checkedItem;
            try
            {
                foreach (ListViewItem item in listView1.Items)
                {
                    checkedItem = item.Checked;

                    //if (item.Checked) { SelectItemAndBrothers(item.Index); }
                //}

                //foreach (int ind in checkedItems)
                //{
                    if (checkedItem) { SelectItemAndBrothers(item.Index); } else { continue; } 

                    dictStrName = item.SubItems[1].Text;
                    candStrName = item.SubItems[0].Text;
                    cof = float.Parse(item.SubItems[2].Text);

                    //dictStrName = listView1.Items[ind].SubItems[1].Text;
                    //candStrName = listView1.Items[ind].SubItems[0].Text;
                    //cof = float.Parse(listView1.Items[ind].SubItems[2].Text);

                    if (myStrDict.TryGetStreet(new Library.Street(dictStrName, ""), out dictStreet) == false)
                        throw new InvalidOperationException(String.Format("Niespodziewany błąd {0} nie istnieje lub jest zdublowany", dictStrName));
                    if (mycandidDict.TryGetStreet(new Library.Street(candStrName, ""), out candStreet) == false)
                        throw new InvalidOperationException(String.Format("Niespodziewany błąd {0} nie istnieje lub jest zdublowany", candStrName));

                    var arr = myStreetPairs[dictStreet].ToArray();

                    //if (myStreetPairs.Contains(candStreet, new Library.StreetWithCof(dictStreet, cof)) == false)
                    //    throw new InvalidOperationException(String.Format("Niespodziewany błąd Słownik nie ma w sobie info a parze {0}, {1} ", candStrName, dictStrName));

                    approvedStreetPairs.Add(candStreet, new Library.StreetWithCof(dictStreet,cof));
                    myStreetPairs.Remove(candStreet);
                }
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(e.ToString(), "Niespodziewany błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception e2)
            {
                MessageBox.Show(e2.ToString(), "Niespodziewany błąd!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Zaznacza wszystkie itemy na liscie o takiej samej
        /// nazwie( kandydata) jak nazwa pod indeksem
        /// </summary>
        /// <param name="itemInd"></param>
        private void SelectItemAndBrothers(int itemInd)
        {
            string key = listView1.Items[itemInd].SubItems[0].Text;
            int count = listView1.Items.Count;
            bool lowBoundFound = false, highBoundFound = false;
            int ind = itemInd;
            int lowBound = ind, highBound=ind;
            int currLowBound, currHighBound;

            for (int i = 0; i < count; i++)
            {
                if (lowBoundFound && highBoundFound)
                    break;
                currLowBound = ind - i;
                currHighBound = ind + i;
                if (currLowBound < 0)
                {
                    lowBoundFound = true;
                    lowBound = 0;
                }
                if (currHighBound > count -1)
                {
                    highBoundFound = true;
                    highBound = count - 1;
                }
                if (!lowBoundFound && listView1.Items[currLowBound].SubItems[0].Text != key)
                {
                    lowBoundFound = true;
                    lowBound = currLowBound + 1;
                }
                if (!highBoundFound && listView1.Items[currHighBound].SubItems[0].Text != key)
                {
                    highBoundFound = true;
                    highBound = currHighBound - 1;
                }

            }
            for (int i = lowBound; i <= highBound; i++)
            {
                listView1.SelectedIndices.Add(i);
            }               
        }

        private void ClearSelected()
        {
            foreach(ListViewItem item in listView1.Items)
            {
                if (item.Selected)
                {
                    listView1.Items.Remove(item);
                    myStreetPairs.Remove(new Library.Street(item.SubItems[0].Text,""));
                }
            }
        }

        private void ProgressBarEvent(int value, int max, int step)
        {
            toolStripProgressBar1.Step = step;
            toolStripProgressBar1.Maximum = max;
            toolStripProgressBar1.Value = value;
        }
       
        // This method is executed on the worker thread and makes
        // a thread-safe call on the TextBox control.
        private void ThreadProcSafe()
        {
            //this.PerformSafeStep();
        }

        private void ProgressBarStepEvent()
        {
            howManyCalled++;
            //this.demoThread =
            //  new Thread(new ThreadStart(this.ThreadProcSafe));

            //this.demoThread.Start();
            //ThreadProcSafe();

            //toolStripProgressBar1.ProgressBar.InvokeRequired
            //lock(toolStripProgressBar1.ProgressBar)
               toolStripProgressBar1.PerformStep();
        }
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 aB = new AboutBox1();
            aB.ShowDialog();
        }

        private void LoadDictionaryMenuItem_Click(object sender, EventArgs e)
        {
            LoadDataFromExcel();
        }

        private void LoadCandidatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CandidateLoad();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void gOToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ComputeStreetDictionary();
        }

        private void saveToFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WriteChanges();
            MessageBox.Show("Zapisano do pliku", "Sukces", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
        }
        /// <summary>
        /// zdarzenie na Item_checked
        /// Dodaje zaznaczone elementy do tablicy Hashującej 
        /// zaznaczonych elementów
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
                checkedItems.Add(e.Item.Index);
            else
                checkedItems.Remove(e.Item.Index);

        }
        /// <summary>
        /// Przejscie do nastepnego najlepszego 
        /// dopsowania na liscie
        /// </summary>
        private void SelectNextEntryInList(bool down)
        {
            if (listView1.Items.Count == 0 || listView1.SelectedIndices.Count == 0)
                return;
            int ind = listView1.SelectedIndices[0];
            if( ( down &&  ind == listView1.Items.Count-1 ) || ( !down &&  ind == 0) )
                return;

            int count = listView1.Items.Count;
            
            string currName = listView1.Items[ind].SubItems[0].Text;
            int increment = down ? 1 : -1;
            int chosenInd = ind;
            if (down)//znajdz OSTATNI pasujacy +1
            {
                for (int i = ind; i < count; i = i + increment)
                {
                    if (listView1.Items[i].SubItems[0].Text != currName)
                    {        
                        chosenInd = i;
                        break;
                    }
                }
                //jesli i przelecialo petle bez break to pod chosenInd pozostaje nadal oryginalny, startowy 
                //index. I tak ma byc...
                listView1.SelectedIndices.Clear();
                listView1.FocusedItem = listView1.Items[chosenInd];
                listView1.SelectedIndices.Add(chosenInd);
                listView1.TopItem = listView1.Items[chosenInd];
            }
            else//znajdz PIERWSZY niepasujacy do elementu powyzej jesli jest najlepszy
            {
                bool currentIsBest = false;
                if (currName != listView1.Items[ind - 1].SubItems[0].Text)//zaznaczony element jest najlepszy
                {
                    currName = listView1.Items[ind - 1].SubItems[0].Text;
                    currentIsBest = true;
                }
                for (int i = ind - (currentIsBest ? 1 : 0); i >= 0; i = i + increment)
                {
                    if (listView1.Items[i].SubItems[0].Text != currName || i==0)
                    {
                        chosenInd = i;           
                        break;
                    }
                }
                listView1.SelectedIndices.Clear();
                listView1.FocusedItem = listView1.Items[chosenInd + ((currentIsBest && chosenInd != 0) ? 1 : 0)];
                listView1.SelectedIndices.Add(chosenInd + ((currentIsBest && chosenInd != 0) ? 1 : 0));
                listView1.TopItem = listView1.Items[chosenInd + ((currentIsBest && chosenInd != 0) ? 1 : 0)];
            }
        }
        //zaladuj do Zaaprobowanej listy 
        //wyczysc z ListView to co niepotrzebne
        private void button2_Click(object sender, EventArgs e)
        {
            AddToApproved();
            listView1.MultiSelect = true;
            listView1.SelectedIndices.Clear();

            //foreach (int checkInd in checkedItems)
            //    SelectItemAndBrothers(checkInd);

            foreach (ListViewItem item in listView1.Items)
            {
                if (item.Checked) { SelectItemAndBrothers(item.Index); }
            }

            ClearSelected();
            checkedItems.Clear();
            listView1.MultiSelect = false;
            listView1.Focus();
        }

        private void listView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (Char)13)
                SelectNextEntryInList(true);
            if(e.KeyChar == (Char)10)
                SelectNextEntryInList(false);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            Console.WriteLine("Form1_FormClosing");
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void candidateLabel_Click(object sender, EventArgs e)
        {

        }  
    }
}
