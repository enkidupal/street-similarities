﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace StreetSimilarities.Library
{
    //klasa odpowiadajaca za laczenie sie z excelem
    public static class DBConnections
    {
        public static DataSet GetExcel(string fileName)
        {
            Application oXL;
            Workbook oWB;
            Worksheet oSheet;
            Range oRng;
            try
            {
                //  creat a Application object
                oXL = new ApplicationClass();
                //   get   WorkBook  object
                oWB = oXL.Workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value);

                //   get   WorkSheet object 
                oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.Sheets[1];
                System.Data.DataTable dt = new System.Data.DataTable("Streets");
                DataSet ds = new DataSet();
                ds.Tables.Add(dt);
                DataRow dr;
                StringBuilder sb = new StringBuilder();
                int jValue = oSheet.UsedRange.Cells.Columns.Count;
                int iValue = oSheet.UsedRange.Cells.Rows.Count;
                //  get data columns
                string[] columnNames = new string[jValue];
                for (int j = 1; j <= jValue; j++)
                {
                    columnNames[j - 1] = ((Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, j]).Text.ToString();
                    dt.Columns.Add(columnNames[j - 1], System.Type.GetType("System.String"));
                }

                //string colString = sb.ToString().Trim();
                //string[] colArray = colString.Split(':');

                //  get data in cell
                for (int i = 2; i <= iValue; i++)
                {
                    dr = ds.Tables["Streets"].NewRow();
                    for (int j = 1; j <= jValue; j++)
                    {
                        oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[i, j];
                        string strValue = oRng.Text.ToString();
                        dr[columnNames[j-1]] = strValue;
                    }
                    ds.Tables["Streets"].Rows.Add(dr);
                }
                return ds;
            }
            catch (Exception ex)
            {  
                return null;
            }
            finally
            {
                
            }
        }
        //wstaw do bazy ListeUlic
        public static void InsertTableIntoDB(System.Data.DataTable dt)
        {
            var db = new StreetModelDataContext();
            foreach (DataRow row in dt.Select())
            {
                InsertOrUpdateStreet(Convert.ToInt32(row[0].ToString()),row[1].ToString());
            }
            
        }

        public static void InsertOrUpdateStreet(int id, string nazwa)
        {
           var db = new StreetModelDataContext();

           var q = (from str in db.Streets
                   where str.street_id == id
                   select str).ToArray();
           if (q.Length != 0)
               return;
           Street st = new Street { street_id = id, streetName = nazwa };
           db.Streets.InsertOnSubmit(st);
           db.SubmitChanges(); 
           
        }

    }
}
