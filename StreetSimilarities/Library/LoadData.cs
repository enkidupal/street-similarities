﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.IO;
using Wintellect.PowerCollections;

namespace StreetSimilarities.Library
{
    //klasa odpowiadajaca za laczenie sie z excelem
    //pobieraniem nazw ulic z pliku CSV
    public static class LoadData
    {
        public static event ProgressBarEventHandler ProgressBarEvent;
        public static event ProgressBarEventStepHandler ProgressBarStepEvent;

        //get Street Names and Load it into Dictionary
        public static StreetDictonary GetExcel(string fileName)
        {
            if (fileName.EndsWith(".csv", true, System.Globalization.CultureInfo.CurrentCulture))
                return GetStreetFromCSV(fileName);
            int MaxColumnToSeek = 20;
            int streetColumnNum = -1;
            bool isColumnWithID = false;
            StreetDictonary streetHash = new StreetDictonary();
            Application oXL;
            Workbook oWB;
            Worksheet oSheet;
            Range oRng;
            Range oRng2;
            oXL = new ApplicationClass();
            try
            {
                //  creat a Application object
                oXL = new ApplicationClass();
                //   get   WorkBook  object
                oWB = oXL.Workbooks.Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value,
                        Missing.Value, Missing.Value);

                //   get   WorkSheet object 
                oSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWB.Sheets[1];
                System.Data.DataTable dt = new System.Data.DataTable("Streets");

                StringBuilder sb = new StringBuilder();
                int jValue = oSheet.UsedRange.Cells.Columns.Count;//2
                int iMaxRows = oSheet.UsedRange.Cells.Rows.Count; //many
                int ii = 0;
                int max = iMaxRows;
                int step = max / 100;
                string ID, strName;
                int oldSize, newSize;
                ProgressBarEvent(ii, max, step);

                for (int j = 1; j <= MaxColumnToSeek; j++)//sprawdz w ktorej kol. jest Ulica
                {
                    if (((Microsoft.Office.Interop.Excel.Range)oSheet.Cells[1, j])
                        .Text.ToString().ToLower().Trim().Contains("ulica"))
                    {
                        streetColumnNum = j;
                        break;
                    }
                }
                if (streetColumnNum == -1)
                    throw new InvalidDataException("Brak kolumny z ulicami");
                else { isColumnWithID = true; }
                Console.WriteLine("" + DateTime.Now + " iMAxRows:" + iMaxRows + ", streetColumnNum:" + streetColumnNum);
                for (int rowI = 2; rowI <= iMaxRows; rowI++)
                {
                    //ProgressBarStepEvent();

                    oRng = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[rowI, streetColumnNum];//Nazwa
                    strName = oRng.Text.ToString().Trim();
                    if (isColumnWithID)
                    {
                        oRng2 = (Microsoft.Office.Interop.Excel.Range)oSheet.Cells[rowI, 1];//Id
                        ID = oRng2.Text.ToString().Trim();
                        
                        Street street = new Street(strName, ID);
                        Street outStreet;
                        if(streetHash.streetNames.Contains(street)) {
                            //streetHash.StreetNames.
                            streetHash.TryGetStreet(street, out outStreet);
                            Console.WriteLine("" + DateTime.Now + " rowI:" + rowI + ", street: " + street + ", " + outStreet);
                        }
                          
                        oldSize = streetHash.streetNames.Count;
                        streetHash.AddStreet(new Street(strName, ID));
                        newSize = streetHash.streetNames.Count;
                        if (oldSize == newSize)
                        {
                            Console.WriteLine("" + DateTime.Now + " rowI:" + rowI + " ID:" + ID + " strName: " + strName + ", oldSize: " + oldSize + ", newSize:" + newSize);
                        }
                    }
                    //else
                    //    streetHash.AddStreet(new Street(strName, ""));
                }
                oXL.Quit();
                ProgressBarEvent(0, 100, 1);
                return streetHash;
            }
            catch (Exception e)
            {
                ProgressBarEvent(0, 100, 1);
                oXL.Quit();
                throw e;
            }
            finally
            {
                Console.WriteLine("beforeQuit(): " + DateTime.Now);
                ProgressBarEvent(0, 100, 1);
                oXL.Quit();
                Console.WriteLine("afterQuit(): " + DateTime.Now);
            }
        }
        //get street from CSV
        public static StreetDictonary GetStreetFromCSV(string fileName)
        {
            StreetDictonary StreetNames = new StreetDictonary();
            bool isColumnWithId = false;
            try
            {
                int culumnStreetNumber = -1;
                int count = 0;
                int lineCount;
                string[] cut;
                using (StreamReader rdr = new StreamReader(fileName, Encoding.GetEncoding(1250)))
                {
                    string line = rdr.ReadLine();
                    cut = line.Split(';');
                    count = cut.Length;
                    for (int i = 0; i < count; i++)
                    {
                        if (cut[i].ToLower() == "ulica")
                        {
                            culumnStreetNumber = i;
                            break;
                        }
                    }
                    if (culumnStreetNumber == -1)
                        throw new Exception("Brak kolumny z ulicami");
                    if (culumnStreetNumber != 0)
                        isColumnWithId = true;
                    lineCount = 1;
                    while ((line = rdr.ReadLine()) != null)
                    {
                        lineCount++;
                        if ((cut = line.Split(';')).Length == count)
                        {
                            if (!isColumnWithId)
                                StreetNames.AddStreet(new Street(cut[culumnStreetNumber].Trim(), ""));
                            else
                                StreetNames.AddStreet(new Street(cut[culumnStreetNumber].Trim(), cut[0].Trim()));
                        }
                        else
                            throw new Exception(String.Format("\nBłąd formatu w pliku CSV:{0} \n(NIEWŁAŚCIWA LICZBA KOLUMN)\n Linia w pliku :{1} Czytany element: {2}", fileName, lineCount, line));
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return StreetNames;
        }
        //load Data to File
        public static void LoadToCSV(string oldFileName, OrderedMultiDictionary<Street, Library.StreetWithCof> dictData,
                                    OrderedMultiDictionary<Street, Library.StreetWithCof> rawData,
                                bool outputOnly100IfPoss, float cutOffparam, int maxCountPerCand)
        {
            OrderedMultiDictionary<Street, Library.StreetWithCof> preparedDict = new OrderedMultiDictionary<Street, StreetWithCof>(true);
            OrderedMultiDictionary<Street, Library.StreetWithCof> rawPreparedDict = new OrderedMultiDictionary<Street, StreetWithCof>(true);

            StringBuilder sb100 = new StringBuilder();
            StringBuilder sbNot100 = new StringBuilder();
            StringBuilder sbNot100Raw = new StringBuilder();

            preparedDict = CutOffUnneededData(dictData, outputOnly100IfPoss, cutOffparam, maxCountPerCand);
            try
            {
                StreamWriter wr100 = new StreamWriter(oldFileName.Insert(oldFileName.LastIndexOf(".",
                    StringComparison.CurrentCultureIgnoreCase), "_OUTPUT_100").Replace("xls", "csv"), false, Encoding.GetEncoding(1250));
                StreamWriter wrNot100 = new StreamWriter(oldFileName.Insert(oldFileName.LastIndexOf(".",
                    StringComparison.CurrentCultureIgnoreCase), "_OUTPUT_POLACZONE").Replace("xls", "csv"), false, Encoding.GetEncoding(1250));
                StreamWriter wrNot100Raw = new StreamWriter(oldFileName.Insert(oldFileName.LastIndexOf(".",
                   StringComparison.CurrentCultureIgnoreCase), "_OUTPUT_NIEPOLACZONE").Replace("xls", "csv"), false, Encoding.GetEncoding(1250));

                sb100.AppendLine("ID_K" + ";" + "Ulica" + ";" + "ID_D" + ";" + "DictEntry");
                sbNot100.AppendLine("ID_K" + ";" + "Ulica" + ";" + "ID_D" + ";" + "DictEntry" + ";Coefficent");
                sbNot100Raw.AppendLine("ID_K" + ";" + "Ulica" + ";" + "ID_D" + ";" + "DictEntry" + ";Coefficent");

                StreetWithCof[] arr;
                foreach (var key in preparedDict.Keys)
                {
                    arr = preparedDict[key].ToArray();
                    if (outputOnly100IfPoss && Math.Abs(arr.First().cof - 1.0F) < Library.NameComparer.epsilon)
                    {
                        sb100.AppendLine(String.Format("{0};{1}", key, arr.First().street));
                    }
                    else
                        sbNot100.AppendLine(String.Format("{0};{1};{2}", key, arr.First().street, arr.First().cof.ToString("0.0000")));

                }
                rawPreparedDict = CutOffUnneededData(rawData, outputOnly100IfPoss, cutOffparam, maxCountPerCand);
                foreach (var key in rawPreparedDict.Keys)
                {
                    arr = rawPreparedDict[key].ToArray();
                    foreach (var strPair in arr)
                    {
                        sbNot100Raw.AppendLine(String.Format("{0};{1};{2}", key, strPair.street, strPair.cof.ToString("0.0000")));
                    }
                }
                wr100.Write(sb100);
                wrNot100.Write(sbNot100);
                wrNot100Raw.Write(sbNot100Raw);
                wr100.Close();
                wrNot100.Close();
                wrNot100Raw.Close();
            }
            catch (Exception e)
            {
                throw e;
            }

        }
        public static OrderedMultiDictionary<Street, Library.StreetWithCof> CutOffUnneededData(
                      OrderedMultiDictionary<Street, Library.StreetWithCof> dictData,
                       bool outputOnly100IfPoss, float cutOffparam, int maxCountPerCand)
        {
            OrderedMultiDictionary<Street, Library.StreetWithCof> result = new OrderedMultiDictionary<Street, StreetWithCof>(true);
            StreetWithCof[] arr;
            float bestCof, currCutOff;
            int count = 0;
            foreach (var StrPairWithCoeff in dictData)
            {
                count = 0;
                arr = dictData[StrPairWithCoeff.Key].ToArray();
                bestCof = arr[0].cof;
                currCutOff = cutOffparam * bestCof;
                if (outputOnly100IfPoss && Math.Abs(arr.First().cof - 1.0F) < Library.NameComparer.epsilon)
                    result.Add(StrPairWithCoeff.Key, arr[0]);//new StreetWithCof(arr[0].street, arr[0].cof));
                else
                    foreach (var strPair in dictData[StrPairWithCoeff.Key].ToArray())
                    {
                        if (strPair.cof < currCutOff)
                            break;
                        if (count++ >= maxCountPerCand)
                            break;
                        result.Add(StrPairWithCoeff.Key, strPair);
                    }
            }
            return result;
        }
    }
}
