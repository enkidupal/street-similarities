﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using System.Globalization;
using Wintellect.PowerCollections;

namespace StreetSimilarities.Library
{
    //klasa odpowiadająca za porównywanie ciągów nazw i sprawdzanie czy nie
    //wystąpiły przekłamania
    public static class NameComparer
    {
        public volatile static int t = 0;
        public static float epsilon = 0.001F;
        public static float punishForDistinctNumOfParts = 0.1F;
        static MultiDictionary<string, string> abbrReplace;
        //to init Abbreviation Dictionary
        static NameComparer()
        {
            splitChars = new char[2]{' ', '.'} ;
            abbrArray = new string[10,2]{ {"generała","gen. "}, {"księdza","Ks. "},{"pl.","plac "},
                             {"świętego","św."},{"świętej","św. "}, {"majora","mjr. "},
                             {"al.","aleja "},{"al.","aleje "},{"podpułkownika","pdpłk. "},
                             {"pułkownika","płk. "} };

            abbrReplace = new MultiDictionary<string, string>(true);
            for(int i=0;i<abbrArray.GetLength(0);i++)
            {
                abbrReplace.Add(abbrArray[i, 0], abbrArray[i, 1]);
            }
        }

        static string expr = @"(?<Name>[^()]+)(?<Local>\(.+\))?";
        static char[] splitChars=null;
        static string[,] abbrArray = null;

        static float CoefBonus = 1.5F; // Bonus z jakim obliczamy pod uwage 
                                           // dopasowanie coef := pow(coef,1/LocalCoeffBonus)
        public static event ProgressBarEventHandler ProgressBarEvent;
        public static event ProgressBarEventStepHandler ProgressBarStepEvent;


        public static float Match(string str1, string str2, float cutoff)
        {
            //cutoff = 0.0F;//czy mozna nizej ?
            float result = 1.0F;
            
            string strName1="";
            string strLocal1="";
            string strName2 = "";
            string strLocal2 = "";
            
            var m1 = Regex.Match(str1, expr);
            var m2 = Regex.Match(str2, expr);
            if (m1.Success)
            {
                strName1 = m1.Groups["Name"].Value.Trim();
                strLocal1 = m1.Groups["Local"].Value.Trim();
            }
            if (m2.Success)
            {
                strName2 = m2.Groups["Name"].Value.Trim();
                strLocal2 = m2.Groups["Local"].Value.Trim();
            }      
            int comp = StringDist(strLocal1, strLocal2);
            //mamy dystans miedzy nazwami (Local1) (Local2)
            //jesli ( (W), null ) to nie sa to podobne nazwy
            result *= getCoeffInDistrictName(comp, strLocal1, strLocal2);
            if (result < cutoff)
                return result; //nie trzeba liczyc dalej
            string[] czl1 = strName1.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
            string[] czl2 = strName2.Split(splitChars, StringSplitOptions.RemoveEmptyEntries); 

            //int minDist=Math.Max(str1.Length,str2.Length);//minimalny dystans
            float maxCoeff = 0.0F;//max dopasowanie
            float Summarycoeff = 1.0F;
            int bonusForExactCzlMatch = 1;

            //teraz obliczyc podobienstow wyrazow wchodzacych w skład nazwy ulicy
            //dla kazdego czlonu z 1 stringu znajdz najlepsze dopasowanie w drugim stringu
            foreach(string n11 in czl1)
            {
                maxCoeff = 0.0F; // dla nowego slowa ustawiamy na nowo dopasowanie ma 0
                string n1 = n11.Trim();
                foreach (string n22 in czl2)
                {
                    string n2 = n22.Trim();
                    if((n1.Length !=n2.Length) && (n1.StartsWith(n2) || n2.StartsWith(n1)) ) //jesli jest podciag
                        maxCoeff =Math.Max(maxCoeff,0.8F); 
                    maxCoeff = Math.Max(maxCoeff, GetCoeff(StringDist(n1, n2),
                                            n1, n2));
                    
                }//end foreach (string n2 in czl2)
                if (Math.Abs(maxCoeff - 1.0F) < Library.NameComparer.epsilon)
                    bonusForExactCzlMatch++;
                Summarycoeff *= maxCoeff;
                if (Summarycoeff < cutoff)
                    return result * Summarycoeff; //nie trzeba liczyc dalej
            }//end foreach(string n1 in czl1)
            result *= Summarycoeff;
            result = result * (Math.Max(0.1F, 1 - NameComparer.punishForDistinctNumOfParts * Math.Abs(czl1.Length - czl2.Length)));//kara za różną liczbę członów
            result = (float)Math.Pow((double)result, 1.0 / (bonusForExactCzlMatch));
            return result;
        }
        ///<summary>
        ///oblicz podobienstwo miedzy dwoma slowami(bez spacji)
        ///Na podstawie Levenshtein distance
        ///http://en.wikipedia.org/wiki/Levenshtein_distance
        public static int StringDist(string s1, string s2)
        {
            int result = 0;
            int n = s2.Length;
            int m = s1.Length;
            if (n == 0 || m == 0)
                return Math.Max(n, m);
            //oblicz min. ilosc operacji(Add, Delete, Substitute) 
            //potrzebnych do trasformacji 's1' w 's2'    
            int[,] d = new int[m, n];
            d[0,0]=0;
            //Tablica d o m+1 wierszach i n+1 kolumnach
            //i--->wiersze ; j--->kolumny
            
            for (int i = 1; i < m; i++)
                d[i, 0] = i;
            for (int j = 1; j < n; j++)
                d[0, j] = j;
            for (int j = 1; j < n; j++)
            {
                for (int i = 1; i < m; i++)
                {
                    if (s1[i - 1] == s2[j - 1])
                        d[i, j] = d[i - 1, j - 1];
                    else
                        d[i,j] = Math.Min(Math.Min(d[i-1,j]+1,d[i,j-1]+1),d[i-1,j-1]+1);
                }                                  //Delete   //Insert    //Substitute
            }
            result = d[m-1, n-1]; 
            return result;
        }

        /// <summary>
        /// Compute Coeff from Levenshtein Distance beetween strings and their lengths
        /// Generally, the longer the strings the higher distance is allowed
        /// </summary>
        /// <param name="dist">Levenshtein distance beetween Strings in  </param> 
        /// <param name="strLength1">Length of 1 string </param>
        /// <param name="strLength2">Length of 2 string</param>
        /// <returns></returns>
        public static float GetCoeff(int dist, string str1, string str2 )
        {
            int strLength1 = str1.Length;
            int strLength2=  str2.Length;

            if (dist == 0)
                return 1.0F;
            if (strLength1 == 0 && strLength2 == 0)
                return 1.0F;
            if(strLength1 ==0)
                return (float)(1 - 0.1)/strLength2;
            if (strLength2 == 0)
                return (float)(1 - 0.1) / strLength1;

            return (float)Math.Pow(Math.Max((float)(strLength1-dist) / strLength1,
                            (float)(strLength2 - dist) / strLength2), 1/CoefBonus);      
        }
        //compute coefficent between two District Names
        //
        public static float getCoeffInDistrictName(int dist, string str1, string str2)
        {
            int strLength1 = str1.Length;
            int strLength2 = str2.Length;
            float result = 1.0F;
            if (dist == 0)
                return 1.0F;
            if (strLength1 == 0 && strLength2 == 0)
                return 1.0F;
            if (strLength1 == 0)
                return 0.8F;
            if (strLength2 == 0)
                return 0.8F;
            int maxL = Math.Max(strLength1, strLength2);
            result *= ((float)Math.Pow((float)dist/maxL ,CoefBonus));
            return result;
        }
        //Get Array of sorted pair of streets with similarity Coefficent
        //Cutoff by <0,1>  0 --> All 1---> Only Identical
        /// <summary>
        /// Get Array of sorted pair of streets with similarity Coefficent
        /// Cutoff by <0,1>  0 --> All 1---> Only Identical
        /// </summary>
        /// <param name="streetDictonary"></param>
        /// <param name="candidateNames"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static OrderedMultiDictionary<Street, StreetWithCof> GetStreetPairsWithCoefficent(
                            Library.StreetDictonary streetDictonary, Library.StreetDictonary candidateNames, 
                            float cutoff, float absoluteCutoff)
        {
            //NameComparer.t = 0;
            if (cutoff < 0 || cutoff > 1)
                return null;

            float weakCutoff = cutoff * cutoff;

            //PrepareCandidate(candidateNames);
            OrderedMultiDictionary<Street, StreetWithCof> result = new OrderedMultiDictionary<Street, StreetWithCof>(true);
            //OrderedMultiDictionary<Street, StreetWithCof> tempResult;
            OrderedSet<StreetWithCof> tempResult;

            //int num = 0;
            int max = candidateNames.streetNames.Count * streetDictonary.streetNames.Count;
            int step = max / 1000;
            //ProgressBarEvent(num, max,step);
            float cof=0.0F;
            List<Street> preparedStreet = new List<Street>();
            Street tempStr;
            bool foundExact = false;
            foreach (var candidate in candidateNames.streetNames)
            {
                NameComparer.t++;
                foundExact = false;
                //ProgressBarStepEvent();
                preparedStreet = PrepareStreetName(candidate);
                //dla kazdego wariantu rozwiniecia kandydata sprawdz czy istnieje dokladny
                //odpowiednik w slowniku
                foreach (Street currVariation in preparedStreet)
                {
                    if (streetDictonary.streetNames.Contains(currVariation))
                    {
                        streetDictonary.TryGetStreet(currVariation,out tempStr);
                        result.Add(candidate,
                            new StreetWithCof(tempStr, 1.0F));
                        foundExact = true;
                        break;
                    }
                }
                if (foundExact)
                    continue;
                //teraz wewnetrzna petla po wpisach w slowniku
                //tempResult = new OrderedMultiDictionary<Street, StreetWithCof>(true);
                tempResult = new OrderedSet<StreetWithCof>(); 
                foreach (var dictStreet in streetDictonary.streetNames)
                {  
                    cof = 0.0F;
                    //dla kazdego wariantu nazwy
                    foreach (Street currVariation in preparedStreet)
                    {
                        //if (NameComparer.t++ % step == 0)
                            //ProgressBarStepEvent();
                        cof = Math.Max(Match(currVariation.streetName, dictStreet.streetName.ToLower().Trim(), absoluteCutoff), cof);
                        if (cof < cutoff)
                            continue;       
                    }
                    if (cof < weakCutoff)
                        continue;
                    tempResult.Add(new StreetWithCof(dictStreet, cof)); 
                    
                    //if (cof < cutoff)
                    //    continue; 
                    //result.Add(candidate, new StreetWithCof(dictStreet, cof)); 
                }
                bool begin = true;
                foreach (var entry in tempResult)
                {
                    if(begin) {
                        result.Add(candidate, entry);
                        begin = false;
                        continue;
                    }
                    if (entry.cof >= cutoff)
                    {
                        result.Add(candidate, entry);
                    }
                }
            }
            //ProgressBarEvent(0, 100,1);
            return result;
        }

        //prepare string before doing magic
        //non-universal part of algorithm
        //if many possible substitutions are Possible
        //Return all posssible Replaced strings
        public static List<Street> PrepareStreetName(Street unPrepared)
        {
            StringBuilder prepared=new StringBuilder(unPrepared.streetName.ToLower().Trim());
            List<Street> result = new List<Street>();

            List<StringBuilder> tempB = new List<StringBuilder>();
            //multi przypadek
            if(unPrepared.streetName.Contains("al.") )
            {
                tempB.Add(new StringBuilder(unPrepared.streetName.Trim().ToLower().Replace("al.", "aleja ")) );
                tempB.Add(new StringBuilder(unPrepared.streetName.Trim().ToLower().Replace("al.", "aleje ")));
            }
            else
                tempB.Add(prepared);
            for (int i = 0; i < abbrArray.GetLength(0); i++)
            {
                foreach (var s in tempB)
                    s.Replace(abbrArray[i, 0], abbrArray[i, 1]);
            }
            foreach (var s in tempB)
                result.Add(new Street(s.ToString(),unPrepared.id));
            return result;
        }

        private static string AbbrConvert(Match m)
        {
            string x = m.ToString();
            return x;
        }
    }
   
    
    
}
