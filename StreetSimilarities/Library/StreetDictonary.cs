﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wintellect.PowerCollections;

namespace StreetSimilarities.Library
{
    
    public static class Helper {

        public static UInt64 CalculateHash(string read)
        {
            UInt64 hashedValue = 3074457345618258791ul;
            for (int i = 0; i < read.Length; i++)
            {
                hashedValue += read[i];
                hashedValue *= 3074457345618258799ul;
            }
            return hashedValue;
        }

        public static bool areNear(float x1, float x2, float eps)
        {
            return Math.Abs(x1 - x2) < eps;
        }
    }

    //klasa przechowujaca info o nazwach ulic 
    //w Slowniku
    public class StreetDictonary:ICloneable
    {

        //set containing info on Streets
        public Set<Street> streetNames;
        public StreetDictonary()
        {
            //StreetNames = new Set<Street>(new StreetComparer());
            streetNames = new Set<Street>();
        }

        public StreetDictonary(IEnumerable<Street> sequence)
        {
            streetNames = new Set<Street>(sequence);
        }

        public System.Object Clone()
        {
            StreetDictonary objReturn = new StreetDictonary();
            objReturn.streetNames = streetNames.Clone();
            return objReturn;
        }
        public bool TryGetStreet(Street toGet, out Street result)
        {
            return streetNames.TryGetItem(toGet, out result);
        }

        public bool AddStreet(Street addedStr)
        {
            if(addedStr.streetName.Length !=0)
                return streetNames.Add(addedStr);
            return false;
        }
        public bool RemoveStreet(string strName)
        {
            return streetNames.Remove(new Street(strName, ""));
        }

        public bool RemoveStreet(Street removeStr)
        {
            return streetNames.Remove(removeStr);
        }

        public void DeleteAll()
        {
            streetNames.Clear();
        }
        
    }

    public class Street:IComparable
    {
        public string streetName {get;set;}
        public string id { get; set; }

        public UInt64 hash { get; set; }

        public Street(string streetParam, string IDParam)
        {
            streetName = streetParam;
            id = IDParam;
            hash = Helper.CalculateHash(streetName.ToLowerInvariant());
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Street))
                return false;
            //return this.streetName.Equals( ((Street)obj).streetName);
            return this.hash.Equals(((Street)obj).hash);
        }

        public override int GetHashCode()
        {
            //return this.id.GetHashCode();

            return (int)hash;

            //return StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.id);
            
        }
        public override string ToString()
        {
            return id + ";" + streetName;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj is Street)
                return this.streetName.CompareTo(((Street)obj).streetName);
            else
                throw new ArgumentException(obj.ToString() + " is Not a Street");
        }

        #endregion
    }
    //Jako wartosc w multi-slowniku 
    public class StreetWithCof : IComparable
    {
        public static float epsilon = 0.0000001F;
        public Street street { get; set; }
        public float cof { get; set; }

        public int CompareTo(object obj)
        {
            if (obj is StreetWithCof)
            {
                StreetWithCof otherStreetPair = (StreetWithCof)obj;
                return (-1) * this.cof.CompareTo(otherStreetPair.cof);//malejąco
            }
            else
            {
                throw new ArgumentException("object is not a StreetPair");
            }
        }
        public override bool Equals(object otherSWithCof)
        {
            if (!(otherSWithCof is StreetWithCof)) return false;

            return (this.street == ((StreetWithCof)otherSWithCof).street) &&
                    //(this.cof == ((StreetWithCof)otherSWithCof).cof);
                    Helper.areNear(this.cof, ((StreetWithCof)otherSWithCof).cof, epsilon);

        }

        public override int GetHashCode()
        {
            //return (hash + (ulong)this.cof.GetHashCode()) * 3074457345618258799ul; 
            return (this.street.GetHashCode() ^ this.cof.GetHashCode());
        }

        public override string ToString()
        {
            return base.ToString() + cof.ToString("0.0000") ;
        }

        public StreetWithCof()
        {
            street = new Street("","");
            cof = 0.0F;
        }

        public StreetWithCof(Street s1, float c)
        {
            street = s1;
            cof = c;
        }
    }
    class IgnoreCaseComparer : IEqualityComparer<string>
    {
        public IgnoreCaseComparer() { }
        public bool Equals(string x, string y)
        {
            return (x.ToLower()) == (y.ToLower());
        }
        public int GetHashCode(string x)
        {
            return x.GetHashCode();
        }
    }
    /*
    public class StreetComparer : IEqualityComparer<Street>
    {
        UInt64 hashedValue = 3074457345618258791ul;
        #region IEqualityComparer<Street> Members

        public bool Equals(Street x, Street y)
        {
            return StringComparer.InvariantCultureIgnoreCase.Equals(x.streetName, y.streetName);
        }

        public UInt64 GetHashCode(Street obj)
        {
            return obj.hash;
            //return StringComparer.InvariantCultureIgnoreCase.GetHashCode(obj.id);
        }

        #endregion
    }

    */

}
